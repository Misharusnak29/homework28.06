/* Теоретичні питання 
1.Події в JavaScript — це сигнали, які вказують, що щось відбулося в системі чи у веб-документі. Вони використовуються для реагування на взаємодію користувача з веб-сторінкою або інші дії, що можуть статися в браузері. Приклади подій включають натискання кнопки миші, завантаження сторінки, зміни вмісту форми, тощо. Події дозволяють розробникам робити веб-сторінки інтерактивними та динамічними, виконуючи JavaScript код у відповідь на ці події.

2. click: подія виникає, коли користувач натискає і відпускає кнопку миші.
dblclick: подія виникає при подвійному натисканні кнопки миші.
mousedown: подія виникає, коли кнопка миші натискається.
mouseup: подія виникає, коли кнопка миші відпускається.
mousemove: подія виникає, коли курсор миші рухається.
mouseover: подія виникає, коли курсор миші наводиться на елемент.
mouseout: подія виникає, коли курсор миші залишає межі елемента.

3. 

*/

// Практичні завдання
// 1.

// document.getElementById('btn-click').addEventListener('click', function() {
//     const newParagraph = document.createElement('p');
//     newParagraph.textContent = 'New Paragraph';
//     document.getElementById('content').appendChild(newParagraph);
// });

// 2.

// document.getElementById('btn-input-create').addEventListener('click', function() {
//     const newInput = document.createElement('input');
//     newInput.setAttribute('type', 'text');
//     newInput.setAttribute('placeholder', 'Enter text here');
//     newInput.setAttribute('name', 'newInput');
//     const formSection = document.getElementById('form-section');
//     formSection.insertBefore(newInput, formSection.childNodes[formSection.childNodes.length - 1]);
// });